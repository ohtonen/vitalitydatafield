using Toybox.WatchUi as Ui;
using Toybox.ActivityMonitor as Act;

class VitalityDataFieldView extends Ui.SimpleDataField {

    var activityInfo;

    //! Set the label of the data field here.
    function initialize() {
        label = "Vitality";
    }

    //! Show points between 0-10 based on Average Heartrate and Steps count.
    function compute(info) {
    	activityInfo = Act.getInfo();
    	
        if (info.averageHeartRate > 134 && (info.elapsedTime / 1000 / 60) > 32) {
    		return 10;
    	} else if (info.averageHeartRate > 116 && (info.elapsedTime / 1000 / 60)  > 62) {
    		return 10;
    	} else if (info.averageHeartRate > 116 && (info.elapsedTime / 1000 / 60)  > 32) {
    		return 5;
      	} else if (activityInfo.steps > 7050 && activityInfo.steps < 10050) {
      		return 3;
      	} else if (activityInfo.steps > 10050 && activityInfo.steps < 12550) {
      		return 5;
      	} else if (activityInfo.steps > 12550) {
    	    return 10;
    	} else {
    		return 0;
    	}
 
    	
    }

}